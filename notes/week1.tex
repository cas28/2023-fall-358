\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage{times,url,amssymb,amsmath,clrscode3e,graphicx,qtree,hyperref}
\headheight=0pt
\headsep=0pt
\topmargin=0pt
\textheight=9in
\textwidth=6.5in
\oddsidemargin=0in % 1in left and right margins
\parindent=0pt
\parskip=1ex
\sloppy

\newcommand{\floor}[1]{\lfloor{#1}\rfloor}
\newcommand{\ceil}[1]{\lceil{#1}\rceil}
\newcommand{\card}[1]{\left\lvert{#1}\right\rvert}

\begin{document}

\section*{CS358 Principles of Programming Languages -- Spring 2023\\
Week 1: Programs as Data; Concrete and Abstract Syntax}

\emph{Notes authored by Andrew Tolmach, drawing heavily on material provided
by Katie Casamento, Jingke Li, and Mark P. Jones.}

\subsection*{Programs as Data}


To work with programs as data, we need a good data structure.

For example, what data structure could we use to represent this C code?

\begin{verbatim}
for (i = 0; i < 10; i += 1) {
  if (i % 2 == 0) {
    printf("%d", i * 3);
  }
}
\end{verbatim}
          
The most obvious answer is as a string or array of characters. This is the format
in which C programs are stored in files, and it certainly contains everything the
programmer wrote down when they entered the program text from the keyboard.

But strings are not a very convenient data structure for many purposes! For example,
suppose that we want to implement an algorithm to decide how nested the
{\tt printf} call is, i.e. how many loops or conditionals it lies inside.
(This might not seem like a very useful
algorithm, but it is the sort of thing done by tools that produce software
complexity metrics; try Googling ``cyclomatic complexity.'')

To compute nesting depth based on the string representation,
we could count curly braces or indentation. (The answer for this program would be 2.)  

But what if ask the same question for the following program?

\begin{verbatim}
for (i = 0; i < 10; i += 1)
if (i % 2 == 0)
printf("%d", i * 3);
\end{verbatim}

This program is perfectly legal C, although its formatting is not the best.
The answer is still 2, but there are no curly braces or indentation to count. Instead,
we have to do a more careful analysis of the text that looks for keywords like {\tt for}
and {\tt if}, and knows that each of these is followed by certain controlling expressions and
then a nested statement. In short, before we can compute the depth of {\tt printf}
we have to figure out the \emph{structure} of the program.  The original program
string gives us enough information to do that; if it didn't, we would have a hard time
processing programs at all! But the process of determining structure, which is called \emph{parsing},
is not trivial.

It turns out that almost anything we do with a program---compiling or interpreting it,
checking it for type errors, even just displaying it using pretty colors in an integrated development environment (IDE)---
requires understanding its structure.  Rather than repeatedly reparsing the program
separately for each task, it makes sense to determine the structure once and for all, and
then represent the program in an internal format that makes its structure obvious.

What is a suitable data structure for representing program structure?
The answer depends in part on the language in question.  For example, assembly code
has very simple structure, so it might be represented as a sequence (i.e. list or array)
of instructions and their associated operands. (Although even here, note that we want
to represent sequences of \emph{instructions}, not just the raw sequence of characters
making up an assembly language file.)
 
But for most modern higher-level languages, the data structure we want needs to
be richer than a sequence, because we must be able to represent \emph{nesting}.  For example, 
the C language allows statements to be composed freely out of smaller statements
using forms like {\tt for} and {\tt if}.  For example, the body of a {\tt for} can
be any kind of statement (including another {\tt for}).
Similarly, C arithmetic expressions, such as {\tt x+2*(y-z)}, are composed
from smaller sub-expressions. For example, the left and right hand of a {\tt +}
operator can themselves be arbitrary expressions (including another {\tt +} expression).
And nearly all modern languages are like C in this regard.

What we need to represent a program with arbitrary nesting is a
\emph{tree}: a data structure consisting of a node connected to
children that are themselves trees. Note that this is a
\emph{recursive} definition, and recursion is often the method of
choice for writing algorithms over trees. This would be a great time
to review what you know about trees from your previous courses in
discrete math, algorithms, etc.

In particular, our example C programs can be represented by this tree:

\includegraphics[scale=0.25]{intro_ast.pdf}\\[-1ex]

\subsection*{Concrete and Abstract Syntax}

Note that this tree represents \emph{either} of our original programs: that is,
the details about curly braces and indentation don't really matter
to the essence of the program.\footnote{
At least most of the time. They do matter to tools that have to communicate
back to the programmer in terms of the original source text, e.g. to report
where an error has occurred.  To support this, we can attach concrete
position information to the AST nodes. In this course, we'll generally ignore this wrinkle. }
We have ``abstracted away from'' these details of C's \emph{concrete syntax},
and so the result is called an \emph{abstract syntax tree (AST)}. AST's are the
foundation for all of the work we do on programs in this course, and in most
real world tools (and academic papers and books). 

The idea of abstracting away unnecessary details of the program is quite general
and can apply to deeper concerns than layout. For example, consider the 
three arithmetic expressions on the left below.

\begin{minipage}{4in}
\begin{verbatim}
(a - b) - c
a - b - c
(a - b - c)
\end{verbatim}
\end{minipage}
\begin{minipage}{4in}
\includegraphics[scale=0.25]{intro_ast_sub.pdf}\\[-1ex]
\end{minipage}

In C (and many other languages), these all have the same behavior when the
program is run: first {\tt b} is subtracted from {\tt a}, and then {\tt c} is
subtracted from the result. 
The first expression makes this explicit using parentheses;
the second relies on a convention that subtraction ``groups to the left'' in the
absence of clarifying parentheses\footnote{More technically, we say that {\tt -} is
\emph{left associative}. We will study associativity and related issues in much more
detail later.}
; the third has useless, but harmless, parentheses.
All these expressions can be represented by the same AST, shown on the right above.
Note that there are no parentheses: instead, the intended structure of the expression
is directly represented by the shape of the tree. Again, that is all we generally care
about. 

In fact, in prinicple, we can abstract away from the concrete syntax of
the program altogether.  For example, the AST above might represent a fragment
of a Scheme program, where arithemetic expressions are written in prefix notation,
e.g. {\tt (- (- a b) c)}, 
or even a fragment of a \href{https://developers.google.com/blockly}{Blockly} program
created with a graphics editor:

\includegraphics[scale=0.4]{blockly.pdf}


A final note: the ASTs described above have been given informally; they are just
pictures. To actually represent ASTs as internal data structures in some implementation
language, we must make choices about how to represent the nodes and edges that
make up the tree. We will take up this question in Homework 1. 

\vfill

\begin{quote}
  In short, ASTs are tree-structured data that represent programs in programs.
  This is a profound idea! In fact, it’s one of the great ideas of the 20th century, building on the brilliant
  work of Gödel (encoding), Turing (universal machine), von Neumann (stored program computer), and McCarthy (metacircular interpreter).
\end{quote}\hfill- Shriram Krishnamurthi, {\it Programming Languages: Application and Interpretation}

\end{document}

